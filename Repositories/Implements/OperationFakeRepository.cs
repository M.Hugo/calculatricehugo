﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CalculatriceAPI.Repositories.Implements
{
    using Models;
    public class OperationFakeRepository: OperationRepository
    {
        private List<OperationModel> entities = new List<OperationModel>(); 
        public IEnumerable<OperationModel> FindAll()
        {
            throw new NotImplementedException();
        }
        public OperationModel Save(OperationModel entity)
        {
            entity.Id = this.entities.Count();
            this.entities.Add(entity);
            return entity
        }
        public void Delete(int id)
        {
            throw new NotImplementedException();
        }
        public OperationModel FindById(int id)
        {
            throw new NotImplementedException();
        }
        public OperationModel Update(OperationModel entity)
        {
            throw new NotImplementedException();
        }
    }
}
