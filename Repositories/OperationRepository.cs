﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CalculatriceAPI.Repositories
{
    using DTO;
    public class OperationRepository
    {
        public IEnumerable<OperationDTO> FindAll();
        public OperationDTO Save(OperationDTO entity);
        public void Delete(int id);
        public OperationDTO FindById(int id);
        public OperationDTO Update(OperationDTO entity);
    }
}
