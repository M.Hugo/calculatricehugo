﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CalculatriceAPI.Services.Implements
{


    using DTO;
    using Repositories;
    using Models;
    public class OperationCalculatriceService : OperationService
    {
        private static List<OperationDTO> operations = new List<OperationDTO>();
        private UtilisateurService utilisateurService;
        private OperationRepository repository;

        public OperationCalculatriceService(UtilisateurService utilisateurService, OperationRepository repository)
        {
            this.utilisateurService = utilisateurService;
            this.repository = repository;
        }
        public IEnumerable<OperationDTO> TrouverTout()
        {
            List<OperationDTO> result = new List<OperationDTO>();
            foreach(OperationDTO op in this.repository.FindAll())
            {
                result.Add(op);
            }
            return result;

        }
        public OperationDTO TrouverUn(int id)
        {
            return operations[id];
        }
        public OperationDTO AjouterUn(OperationDTO data)
        {
            data.Id = operations.Count();
            data.Auteur = this.utilisateurService.TrouverUnUtilisateur(data.Auteur.Id);
            operations.Add(data);
            return data;
        }
        public OperationDTO Modifier(int id, OperationDTO data)
        {
            data.Id = id;
            data.Auteur = this.utilisateurService.TrouverUnUtilisateur(data.Auteur.Id); 
            operations[id] = data;
            return data;
        }
        public void Supprimer(int id)
        {
            operations[id] = null;
        }
    }
}