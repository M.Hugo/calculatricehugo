﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CalculatriceAPI.Services.Implements
{
    using DTO;
    public class UtilisateurCalculatriceService : UtilisateurService
    {
        private static List<UtilisateurDTO> utilisateurs = new List<UtilisateurDTO>();
        public IEnumerable<UtilisateurDTO> TrouverToutUtilisateur()
        {
            return utilisateurs;
        }
        public UtilisateurDTO TrouverUnUtilisateur(int id)
        {
            return utilisateurs[id];
        }
        public UtilisateurDTO AjouterUnUtilisateur(UtilisateurDTO utilisateur)
        {
            utilisateur.Id = utilisateurs.Count();
            utilisateurs.Add(utilisateur);
            return utilisateur;
        }
        public void SupprimerUtilisateur(int id)
        {
            utilisateurs[id] = null;
        }
        public IEnumerable<UtilisateurDTO> TrouverParNom(string nom)
        {
            return utilisateurs.Where(u => u.Nom == nom);
        }
        public IEnumerable<UtilisateurDTO> TrouverParPrenom(string prenom)
        {
            return utilisateurs.Where(u => u.Prenom == prenom);
        }
        public IEnumerable<UtilisateurDTO> TrouverParAge(int age)
        {
            return utilisateurs.Where(u => u.Age == age);
        }
        public IEnumerable<UtilisateurDTO> TrouverParMetier(string metier)
        {
            return utilisateurs.Where(u => u.Metier == metier);
        }
    }
}