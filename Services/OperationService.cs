﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CalculatriceAPI.Services
{
    using DTO;

    public interface OperationService
    {
        //*implementation de l'interface avec les methodes "TrouverUnUtilisateur","TrouverParNom","TrouverParValeur"*//
        public IEnumerable<OperationDTO> TrouverTout();
        public OperationDTO AjouterUn(OperationDTO utilisateur);
        public void Supprimer(int id);
        public OperationDTO TrouverUn(int id);
        public OperationDTO Modifier(int id, OperationDTO nouveau);
    }
}
