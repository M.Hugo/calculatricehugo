﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CalculatriceAPI.Services
{
    using DTO;

    public interface UtilisateurService
    {
        public IEnumerable<UtilisateurDTO> TrouverToutUtilisateur();
        public UtilisateurDTO TrouverUnUtilisateur(int id);
        public UtilisateurDTO AjouterUnUtilisateur(UtilisateurDTO utilisateur);
        public void SupprimerUtilisateur(int id);
        public IEnumerable<UtilisateurDTO> TrouverParNom(string nom);
        public IEnumerable<UtilisateurDTO> TrouverParPrenom(string prenom);
        public IEnumerable<UtilisateurDTO> TrouverParAge(int age);
        public IEnumerable<UtilisateurDTO> TrouverParMetier(string metier);

    }
}
