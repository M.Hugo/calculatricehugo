﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CalculatriceAPI.Controllers
{
    using DTO;
    using Services;
    using Services.Implements;

    [Route("api/operations")]
    [ApiController]
    public class OperationController : ControllerBase
    {
        private OperationService service;

        public OperationController(OperationService service)
        {
            this.service = service;
        }

        [HttpGet]
        public IEnumerable<OperationDTO> FindAll()
        {
            return service.TrouverToutUtilisateur();
        }

        //Fais appel à la methode "TrouverUnUtilisateur" du service "OperationCalculatriceService"
        [HttpGet]
        [Route("{id}")]
        public OperationDTO FindById(int id)
        {
            return service.TrouverUnUtilisateur(id);
        }

        //Fais appel à la methode "TrouverParNom" du service "OperationCalculatriceService"
        [HttpGet]
        [Route("nom/{nom}")]
        public IEnumerable<OperationDTO> TrouverParNom(string nom)
        {
            return service.TrouverParNom(nom);
        }

        //Fais appel à la methode "TrouverParValeur" du service "OperationCalculatriceService"
        [HttpGet]
        [Route("valeur/{valeur}")]
        public IEnumerable<OperationDTO> TrouverParValeur(string valeur)
        {
            return service.TrouverParValeur(valeur);
        }
    }
}
