﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CalculatriceAPI.Controllers
{
    using DTO;
    using Services;
    using Services.Implements;

    [Route("api/utilisateurs")]
    [ApiController]
    public class UtilisationController : ControllerBase
    {
        private UtilisateurService service;

        public UtilisationController(UtilisateurService service)
        {
            this.service = service;
        }

        [HttpGet]
        public IEnumerable<UtilisateurDTO> FindAll()
        {
            return service.TrouverToutUtilisateur();
        }

        [HttpPost]
        public UtilisateurDTO Save([FromBody] UtilisateurDTO value)
        {
            return service.AjouterUnUtilisateur(value);
        }

        [HttpGet]
        [Route("{id}")]
        public UtilisateurDTO FindById(int id)
        {
            return service.TrouverUnUtilisateur(id);
        }

        [HttpPut("{id}")]
        public UtilisateurDTO Update(int id, [FromBody] UtilisateurDTO value)
        {
            return null;
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            service.SupprimerUtilisateur(id);
        }

        [HttpGet]
        [Route("nom/{nom}")]
        public IEnumerable<UtilisateurDTO> TrouverParNom(string nom)
        {
            return service.TrouverParNom(nom);
        }

        [HttpGet]
        [Route("prenom/{prenom}")]
        public IEnumerable<UtilisateurDTO> TrouverParPrenom(string prenom)
        {
            return service.TrouverParPrenom(prenom);
        }

        [HttpGet]
        [Route("age/{age}")]
        public IEnumerable<UtilisateurDTO> TrouverParAge(int age)
        {
            return service.TrouverParAge(age);
        }

        [HttpGet]
        [Route("metier/{metier}")]
        public IEnumerable<UtilisateurDTO> TrouverParMetier(string metier)
        {
            return service.TrouverParMetier(metier);
        }
    }
}
