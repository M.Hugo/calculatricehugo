﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CalculatriceAPI.Controllers
{
    [Route("api/Calculatrice")]
    [ApiController]
    public class CalculatriceController : ControllerBase
    {
        [HttpGet]
        public string Get()
        {
            return "it's ok !";
        }

        [HttpGet]
        [Route("add/{a}/{b}")]
        public string Addition(int a, int b)
        {
            return (a + b).ToString();
        }
        [HttpGet]
        [Route("sous/{a}/{b}")]
        public string Soustraction(int a, int b)
        {
            return (a - b).ToString();
        }
        [HttpGet]
        [Route("mult/{a}/{b}")]
        public string Multiplication(int a, int b)
        {
            return (a * b).ToString();
        }
        [HttpPost]
        public string Post([FromBody] Data content)
        {
            return (content.a + content.b).ToString();
        }
    }
}