﻿using CalculatriceAPI.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CalculatriceAPI.Models
{
    public class OperationModel
    {
        public int Id { get; set; }
        public string Nom { get; set; }
        public string Valeur { get; set; }
        public UtilisateurDTO Auteur { get; set; }
    }
}
